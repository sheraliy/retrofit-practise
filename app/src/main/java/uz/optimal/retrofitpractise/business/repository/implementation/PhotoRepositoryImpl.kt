package uz.optimal.retrofitpractise.business.repository.implementation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.optimal.retrofitpractise.business.data.UIPhoto
import uz.optimal.retrofitpractise.business.mapper.PhotoEntityMapper
import uz.optimal.retrofitpractise.network.data.model.DomainPhoto
import uz.optimal.retrofitpractise.business.repository.abstraction.PhotoRepository
import uz.optimal.retrofitpractise.cache.source.PhotoLocalDaSource
import uz.optimal.retrofitpractise.common.UIState
import uz.optimal.retrofitpractise.network.data.source.abstraction.PhotoNetworkDataSource
import javax.inject.Inject

class PhotoRepositoryImpl @Inject constructor(
    private val photoNetworkDataSource: PhotoNetworkDataSource,
    private val photoLocalDaSource: PhotoLocalDaSource,
    private val photoEntityMapper: PhotoEntityMapper
)
    :PhotoRepository{
    override  fun getPhotos():Flow<UIState<List<UIPhoto>>>{

        return flow {
            val photos = photoLocalDaSource.getAll()
            emit(
                UIState.Success(
                        photoEntityMapper.mapFromEntityList(photos as List<DomainPhoto>),
                )
            )
        }
    }


}