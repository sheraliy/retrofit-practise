package uz.optimal.retrofitpractise.framework

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import uz.optimal.retrofitpractise.business.data.UIPhoto
import uz.optimal.retrofitpractise.business.repository.abstraction.PhotoRepository
import uz.optimal.retrofitpractise.common.Event
import uz.optimal.retrofitpractise.common.UIState
import uz.optimal.retrofitpractise.network.data.model.DomainPhoto
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: PhotoRepository
) :ViewModel() {

    private val _photos = MutableLiveData<UIState<List<UIPhoto>>>()
    val photos: LiveData<UIState<List<UIPhoto>>>
    get() = _photos

    fun getPhotos() {

        repository.getPhotos() .onStart {}
            .catch {
            }
            .onEach {
                _photos.postValue(it)
            }.flowOn(Dispatchers.IO)
            .launchIn(viewModelScope)

    }
}