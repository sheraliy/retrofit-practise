package uz.optimal.retrofitpractise.framework

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_photo.view.*
import uz.optimal.retrofitpractise.R
import uz.optimal.retrofitpractise.business.data.UIPhoto

class MyRecAdapter(var list: ArrayList<UIPhoto>) : RecyclerView.Adapter<MyRecAdapter.VH>() {

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(domainPhoto: UIPhoto) {
            itemView.apply {
                tv_photo.text = domainPhoto.author

                Glide.with(context).load(domainPhoto.downloadUrl).into(iv_photo)

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

}