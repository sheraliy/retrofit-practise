package uz.optimal.retrofitpractise.network.data.source.implementation

import uz.optimal.retrofitpractise.network.api.PhotoService
import uz.optimal.retrofitpractise.network.data.model.DomainPhoto
import uz.optimal.retrofitpractise.network.data.source.abstraction.PhotoNetworkDataSource
import javax.inject.Inject

class PhotoNetworkDataSourceImpl @Inject constructor(
    private val photoService: PhotoService
) :PhotoNetworkDataSource{
    override suspend fun getPhotos(): List<DomainPhoto> {
        return photoService.getAllPhoto()
    }
}