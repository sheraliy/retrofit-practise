package uz.optimal.retrofitpractise.common.mapper

interface EntityMapper<Entity, UIModel> {
    fun mapFromEntity(entity: Entity): UIModel

    fun mapToEntity(uiModel: UIModel): Entity

    open fun mapToEntityList(list: List<UIModel>):List<Entity>{
        return ArrayList<Entity>().apply {
            list.forEach{
                add(mapToEntity(it))
            }
        }
    }

    open fun mapFromEntityList(list: List<Entity>):List<UIModel>{
        return ArrayList<UIModel>().apply {
            list.forEach{
                add(mapFromEntity(it))
            }
        }
    }

}