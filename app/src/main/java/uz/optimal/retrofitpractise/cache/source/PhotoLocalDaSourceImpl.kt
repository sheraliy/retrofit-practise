package uz.optimal.retrofitpractise.cache.source

import uz.optimal.retrofitpractise.cache.database.dao.PhotoDao
import uz.optimal.retrofitpractise.cache.model.EntityPhoto
import javax.inject.Inject

class PhotoLocalDaSourceImpl @Inject constructor(
    private val photoDao: PhotoDao,
) : PhotoLocalDaSource {

    override fun getAll(): List<EntityPhoto> {
        return photoDao.getAll()
    }

    override suspend fun insert(photo: EntityPhoto) {
        return photoDao.insert(photo)
    }

    override suspend fun insertAll(list: List<EntityPhoto>) {
        return photoDao.insertAll(list)
    }

}