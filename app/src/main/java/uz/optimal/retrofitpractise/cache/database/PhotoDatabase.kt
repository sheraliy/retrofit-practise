package uz.optimal.retrofitpractise.cache.database

import androidx.room.Database
import androidx.room.RoomDatabase
import uz.optimal.retrofitpractise.cache.database.dao.PhotoDao
import uz.optimal.retrofitpractise.cache.model.EntityPhoto

@Database(
    entities = [
        EntityPhoto::class
    ],
    views = [],
    version = 5,
    exportSchema = false
)
abstract class PhotoDatabase :RoomDatabase(){

    abstract fun photoDao(): PhotoDao

}