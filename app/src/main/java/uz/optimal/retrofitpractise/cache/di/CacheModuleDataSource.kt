package uz.optimal.retrofitpractise.cache.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uz.optimal.retrofitpractise.cache.source.PhotoLocalDaSource
import uz.optimal.retrofitpractise.cache.source.PhotoLocalDaSourceImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CacheModuleDataSource {

    @Singleton
    @Provides
    fun providePhotoDataSource(
        photoLocalDataSourceImpl: PhotoLocalDaSourceImpl
    ): PhotoLocalDaSource = photoLocalDataSourceImpl

}