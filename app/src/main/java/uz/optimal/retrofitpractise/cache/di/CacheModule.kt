package uz.optimal.retrofitpractise.cache.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import uz.optimal.retrofitpractise.cache.database.PhotoDatabase
import uz.optimal.retrofitpractise.cache.database.dao.PhotoDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)

object CacheModule {

    @Singleton
    @Provides
    fun providePhotoDao(
        photoDatabase: PhotoDatabase
    ):PhotoDao{
        return photoDatabase.photoDao()
    }

    @Singleton
    @Provides
    fun provideUserDatabase(
        @ApplicationContext context: Context
    ): PhotoDatabase {
        return Room.databaseBuilder(context, PhotoDatabase::class.java, "photo_db")
            .fallbackToDestructiveMigration().build()
    }

}